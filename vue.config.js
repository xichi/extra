   module.exports = {
	    lintOnSave:false,
	    devServer:{
	    	// 关闭eslint语法验证
	    	overlay:{
	    		warning:false,
	    		errors:false
	    	},
			// "https" : true,
            "disableHostCheck" : true,
            "proxy" : {
                "/api" : {
                    "target" : "http://xxx.com",
                    "changeOrigin" : true, //跨域
                    "secure" : false
                }
            }
	    },
        publicPath: process.env.NODE_ENV === 'production' ? './' : './'  
   };  